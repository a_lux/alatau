@extends('layouts.app')
@section('content')

    @include('/partials/desktop_menu', ['class' => 'nav nav-about'])
{{--    @include('/partials/breadcrumbs')--}}
    <br><br><br><br><br><br><br>
    <section class="s-company">
        <div class="container">
            <div class="company__inner">
                <h2 class="company__title">{{ $page->getPage()->translate(app()->getLocale())->title }}</h2>
                <p class="company__undertitle">{{ $model->content }}</p>
                

                <!-- <p class="company__lead">@lang('texts.О компании')</p>
                <ul class="company__list">
                    {!! $model->services !!}
                </ul>

                <p class="company__lead">@lang('texts.Сфера деятельности')</p>
                <ul class="company__list">
                    {!! $model->areas_of_activity !!}
                </ul> -->

            </div>
        </div>
    </section>

    @if($graph->count() > 0)
    <section class="s-graphics" style = "background-image: url({{ asset('img/works-back.png') }})">
        <div class="graphics__inner">
            <h2 class="graphics__title">{{ $page->getPage()->getFirstTitle() }}</h2>
            <div class="graphics__block">
                    @foreach($graph as $k => $v)
                        <div class="graphics__item">
                            <div class="graphics__info">
                                <p class="graphics__number count">{{ $v->title }}</p>
                                <p class="graphics__name">{{ $v->subtitle }}</p>
                            </div>
                            <p class="graphics__description">
                                {{ $v->content }}
                            </p>
                        </div>
                    @endforeach    
            </div>
            <!-- <a href="{{ route('project_all') }}" class="btn"><span>@lang('texts.смотреть все проекты')</span></a> -->
        </div>
    </section>
    @endif


    @if($certificates->count() > 0)
    <section class="s-sertificate">
        <div class="container">
            <div class="sertificate__inner">
                <h2 class="sertificate__title">{{ $page->getPage()->getSecondTitle() }}</h2>
                <p class="sertificate__undertitle">{{ $page->getPage()->getSecondSubtitle() }}</p>

                <div class="sertificate-slider">
                    @foreach($certificates as $k => $v)
                        <a href="{{ Voyager::image($v->image) }}" class="slider__item slide">
                            <img src="{{ Voyager::image($v->image) }}" alt="sertificate">
                        </a>
                    @endforeach
                </div>

            </div>
        </div>
    </section>
    @endif

<!-- 
    @if($projects->count() > 0)
    <section class="s-works" style = "background-image: url({{ asset('img/works-back.png') }})">
        <div class="container">
            <div class="works__inner">
                <div class="works__title">{{ $page->getPage()->getThirdTitle() }}</div>
                <div class="works-slider">
                   @include('/partials/projects', ['projects' => $projects])
                </div>
                <a href="{{ route('project_all') }}" class="btn"><span>@lang('texts.смотреть все проекты')</span></a>

            </div>
        </div>
    </section>
    @endif -->

@endsection
