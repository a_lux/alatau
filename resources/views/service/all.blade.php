@extends('layouts.app')
@section('content')


    @include('/partials/desktop_menu', ['class' => 'nav nav-about'])

{{--    <div class="nav-position">--}}
{{--        <div class="container">--}}
{{--            <a href = "{{ route('home') }}">@lang('texts.Главная') ></a> {{ $page->getPage()->title }}--}}
{{--        </div>--}}
{{--    </div>--}}

    <section class="s-services service-page">
        <div class="container">
            <div class="services__inner">
                <h2 class="services__title">{{ $page->getPage()->translate(app()->getLocale())->title }}</h2>

                <div class="services__block">
                    @foreach($model as $k => $v)
                        <!-- <a href = "{{ route('service_inner', ['url' => $v->url]) }}" class="services__item"> -->
                        <a class="services__item">
                            <div class="services__img"><img src="{{ Voyager::image($v->icon) }}" alt=""></div>
                            <div class="services__name">{{ $v->name }}</div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>


@endsection
