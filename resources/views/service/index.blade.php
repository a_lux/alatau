@extends('layouts.app')
@section('content')

    @include('/partials/desktop_menu', ['class' => ''])
    <div class="works-more__block service-more">
        <div class="container">
            <h2 class="works-more__title">
               {{ $model->name }}
            </h2>
            <img src=" {{ Voyager::image($model->icon) }}" alt="">
        </div>
    </div>
    <section class="s-company">
        <div class="container">
            <div class="company__inner">
                {!! $model->content !!}

            </div>
        </div>
    </section>

@endsection
