<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="{{ $page->getMeta()->description }}">
    <meta name="keyword" content="{{ $page->getMeta()->keyword }}">
    <meta name="base" content="{{ url('/') }}">
    <title>{{ $page->getMeta()->title }}</title>

    <link rel="stylesheet" href="{{ asset('/libs/hamburgers.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/libs/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>


<div class="wrapper">

    <div class="header">
        <div class="container">
            <div class="header__inner">
                @if($_SERVER["REQUEST_URI"] == "/")
                    <a href = "#" class="header__logo">
                        <img src="{{ Voyager::image(setting('site.logo')) }}" alt="Logo">
                        <div class="logo__name">Alatau <span>Innovations</span></div>
                    </a>
                @else
                    <a href = "/" class="header__logo">
                        <img src="{{ Voyager::image(setting('site.logo')) }}" alt="Logo">
                        <div class="logo__name">Alatau <span>Innovations</span></div>
                    </a>
                @endif
                <div class="header__links">
                    @if(setting('site.facebook') != null)
                        <a href = "{{ setting('site.facebook') }}" class="header__link facebook"><i class="fab fa-facebook-f"></i></a>
                    @endif
                    @if(setting('site.linkedin') != null)
                        <a href = "{{ setting('site.linkedin') }}" class="header__link linkedin"><i class="fab fa-linkedin-in"></i></a>
                    @endif
                    <div class="header__email"><a href = "mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a></div>
                    <div class="phone-number"><a href="tel:{{ setting('site.phone') }}">{{ setting('site.phone') }}</a></div>
                    <div class="langs">
                        <a class="lang {{ app()->isLocale('kz') ? 'active-lang':'' }}" href="{{ route('lang', ['url' => 'kz']) }}">kz</a>
                        <a class="lang {{ app()->isLocale('en') ? 'active-lang':'' }}" href="{{ route('lang', ['url' => 'en']) }}">en</a>
                        <a class="lang {{ app()->isLocale('ru') ? 'active-lang':'' }}" href="{{ route('lang', ['url' => 'ru']) }}">ру</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hamburger hamburger--collapse">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>
