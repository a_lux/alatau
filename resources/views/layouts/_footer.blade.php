

<footer class="footer">
    <div class="container">
        <div class="footer__inner">
            @include('/partials/footer_menu')
            <div class="footer__links">
                <div class="footer__email"><a href = "mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a></div>
                <div class="phone-number"><a href="tel:{{ setting('site.phone') }}">{{ setting('site.phone') }}</a></div>
                <div class="links__block">
                    @if(setting('site.facebook') != null)
                        <a href = "{{ setting('site.facebook') }}" class="footer__link facebook"><i class="fab fa-facebook-f"></i></a>
                    @endif
                    @if(setting('site.linkedin') != null)
                        <a href = "{{ setting('site.linkedin') }}" class="footer__link linkedin"><i class="fab fa-linkedin-in"></i></a>
                    @endif
                </div>
            </div>
            <div class="copyright">{{ $footer->text }}</div>
            <a class="footer__links alux" href="https://www.a-lux.kz/">@lang('texts.Разработано в') Alux</a>
        </div>
    </div>
</footer>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="{{ asset('/libs/slick.js') }}"></script>
<script src="{{ asset('/libs/magnific-popup.min.js') }}"></script>
<script src="{{ asset('/js/index.js') }}"></script>
{{--<link href="{{ asset('/js/app.js') }}" rel="stylesheet">--}}

</body>
</html>



