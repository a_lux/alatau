@extends('layouts.app')
@section('content')

    @if($banners->count() > 0)
        @include('/partials/desktop_menu', ['class' => 'nav'])
    @else
        @include('/partials/desktop_menu', ['class' => 'nav nav-about'])
    @endif

    @if($banners->count() > 0)
        @include('/partials/banner', compact('banners'))
    @endif

    <section class="s-works">
        <div class="container">
            <div class="works__inner">
                <div class="works__page-title">{{ $page->getPage()->translate(app()->getLocale())->title }}</div>

                <div class="works-block">
                    @include('/partials/projects', ['projects' => $model])
                </div>

            </div>
        </div>
    </section>

@endsection
