@extends('layouts.app')
@section('content')

    @include('/partials/desktop_menu', ['class' => ''])

    <div class="works-more__block">
        <div class="container">
            <h2 class="works-more__title">
               {{ $model->name }}
            </h2>
        </div>
    </div>
    <section class="s-company">
        <div class="container">
            <div class="company__inner">
                <p class="company__lead">@lang('texts.Заказчик'):</p>
                <ul class="company__list">
                    {!! $model->customer !!}
                </ul>

                <p class="company__lead">@lang('texts.Проект'):</p>
                <ul class="company__list">
                    {!! $model->project !!}
                </ul>

                <p class="company__lead">@lang('texts.Цель проекта'):</p>
                <ul class="company__list">
                    {!! $model->objective !!}
                </ul>
                <p class="company__lead">@lang('texts.Результат'):</p>
                <ul class="company__list">
                    {!! $model->result !!}
                </ul>

            </div>
        </div>
    </section>
    <!-- <div class="container">
        <div class="works-more-slider">
            @php $images = json_decode($model->images);  @endphp
            @if($images != null)
                @foreach($images as $k => $v)
                    <div class="slider__item">
                        <img src="{{ Voyager::image($v) }}" alt="">
                    </div>
                @endforeach
            @endif
        </div>
    </div> -->

@endsection
