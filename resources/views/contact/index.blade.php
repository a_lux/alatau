@extends('layouts.app')
@section('content')

    @include('/partials/desktop_menu', ['class' => 'nav nav-about'])
{{--    @include('/partials/breadcrumbs')--}}

    <section class="contact">
        <div class="container">
            <div class="contact__inner">
                <h2 class="contact__title">{{ $page->getPage()->translate(app()->getLocale())->title }}</h2>
            </div>
        </div>
    </section>
    <section class="maps">
        <div class="selectors__maps">
            @foreach($model as $k => $v)
                <div class="selector {{ $loop->first ? "active-select" : "" }}">
                    <h2 class="maps__city">{{ $v->city }}</h2>
                    <p class="maps__address">{{ $v->address }}   {{ $v->telephone }}</p>
                    <a class = "maps__email" href="mailto:{{ $v->email }}">{{ $v->email }}</a>
                </div>
            @endforeach
        </div>
        @php $m = 0 @endphp
        @foreach($model as $k => $v)
            @php $m++ @endphp
            <div class="map-{{ $m }} {{ $loop->first ? "active-map" : "" }}">
                <iframe src="{{ $v->url }}" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
        @endforeach
    </section>

    <div class="flash-message">
        @if(session()->has('request_status'))
            @php session()->remove('request_status') @endphp
            @if(app()->isLocale('ru'))
                <script>
                    Swal.fire( 'Заявка успешно отправлен!' , 'В ближайшее время мы свяжемся с вами.' , 'success');
                </script>
            @elseif(app()->isLocale('en'))
                <script>
                    Swal.fire( 'Request sent successfully!' , 'In the near future we will contact you.' , 'success');
                </script>
            @else
                <script>
                    Swal.fire( 'Өтініш сәтті жіберілді!' , 'Жақын арада сізбен хабарласамыз.' , 'success');
                </script>
            @endif


        @endif
    </div>
    <div class="get-us" style = "background-image: url({{ asset('img/works-back.png') }});" id="contact-us">
        <div class="container">
            <div class="get-us__title">@lang('texts.Свяжитесь с нами')
            </div>
            <form class="get-us__form" method="get" action="/feedback">
                <input type="email" placeholder="Email" name="email" required>
                <textarea cols="30" rows="8" placeholder="@lang('texts.Комментарий')" name="comment"></textarea>
                <button class="btn btn--get-us"><span>@lang('texts.связаться с нами')</span></button>
            </form>
        </div>
    </div>

@endsection
