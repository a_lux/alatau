@extends('layouts.app')
@section('content')

    @if($banners->count() > 0)
        @include('/partials/desktop_menu', ['class' => 'nav'])
    @else
        @include('/partials/desktop_menu', ['class' => 'nav nav-about'])
    @endif

    @if($banners->count() > 0)
        @include('/partials/banner', compact('banners'))
    @endif

    <section class="s-about">
        <div class="container">
            <div class="about__inner">
                <h2 class="about__title">{{ $page->getPage()->getFirstTitle() }}</h2>
                <p class="about__undertitle">
                  {{ $about->first_block }}
                </p>
                <p class="about__text">
                    {{ $about->second_block }}
                </p>
                <!-- <a href="{{ route('contacts') }}#contact-us" class="btn"><span>@lang('texts.связаться с нами')</span></a> -->
            </div>
        </div>
    </section>
    <section class="s-works" style = "background-image: url({{ asset('img/works-back.png') }})">
        <div class="container">
            <div class="works__inner">
                <div class="works__title">{{ $page->getPage()->getSecondTitle() }}</div>

                <div class="works-slider">
                    @include('/partials/projects', ['projects' => $projects])
                </div>
                <a href="{{ route('project_all') }}" class="btn"><span>@lang('texts.смотреть все проекты')</span></a>

            </div>
        </div>
    </section>
    <section class="s-services">
        <div class="container">
            <div class="services__inner">
                <h2 class="services__title">{{ $page->getPage()->getThirdTitle() }}</h2>

                <div class="services__block">
                    @foreach($services as $k => $v)
                        <!-- <a href = "{{ route('service_inner', ['url' => $v->url]) }}" class="services__item"> -->
                        <a class="services__item">
                            <div class="services__img"><img src="{{ Voyager::image($v->icon) }}" alt=""></div>
                            <div class="services__name">{{ $v->name }}</div>
                        </a>
                    @endforeach
                </div>
                <a href="{{ route('service_all') }}" class="btn"><span>@lang('texts.смотреть все услуги')</span></a>
            </div>
        </div>
    </section>

    @if($certificates->count() > 0)
        <section class="s-works" style = "background-image: url({{ asset('img/works-back.png') }})">
            <div class="container">
                <div class="sertificate__inner">
                    <h2 class="sertificate__title" style="color: white;">{{ $page->getPage()->getForthTitle() }}</h2>
                    <p class="sertificate__undertitle" style="color: white;">{{ $page->getPage()->getForthSubtitle() }}</p>

                    <div class="sertificate-slider">
                        @foreach($certificates as $k => $v)
                            <a href="{{ Voyager::image($v->image) }}" class="slider__item slide">
                                <img src="{{ Voyager::image($v->image) }}" alt="sertificate">
                            </a>
                        @endforeach
                    </div>

                </div>
            </div>
        </section>
    @endif

@endsection
