@extends('layouts.app')
@section('content')

    @include('/partials/desktop_menu', ['class' => 'nav nav-about'])
    <section class="s-brands">
        <div class="container">
            <div class="brands__inner">
                <h2 class="brands__title"> {{ $page->getPage()->translate(app()->getLocale())->title }}</h2>

                <div class="brands__block">

                    @foreach($model as $k => $v)
                        <div class="brands__item">
                            <div class="brands__img">
                                @if($v->url != null)
                                    <a href="{{ $v->url }}" target="_blank">
                                        <img src="{{ Voyager::image($v->image) }}" alt=" {{ $v->description }}">
                                    </a>
                                @else
                                    <img src="{{ Voyager::image($v->image) }}" alt=" {{ $v->description }}">
                                @endif
                            </div>
                            <p class="brands__text">
                                {{ $v->description }}
                            </p>
                        </div>
                    @endforeach

                </div>

            </div>
        </div>
    </section>

@endsection
