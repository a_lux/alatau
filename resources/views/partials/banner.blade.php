<div class="main-slider">
    @foreach($banners as $k => $v)
        <div class="slider__item">
            <img src="{{ Voyager::image($v->image) }}" alt="">
            <div class="container">
                <div class="slide__content">
                    <h2 class="slide__title">
                        {{ $v->title }}
                    </h2>
                    <p class="slide__text">
                        {{ $v->content }}
                    </p>
                    @if($v->url != null)
                        <a href="{{ $v->url }}" class="btn"><span>@lang('texts.подробнее')</span></a>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
</div>
