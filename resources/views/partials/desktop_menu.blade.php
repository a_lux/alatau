
@if($class != "")
<nav class="{{ $class }}">
    <div class="container">
        @include('/partials/_menu', ['menu' => menu('site_header', '_json')])
    </div>
</nav>

@else

<nav class="nav works-more">
    <div class="container">
        @include('/partials/_menu', ['menu' => menu('site_header', '_json')])
        @include('/partials/breadcrumbs')
    </div>
</nav>

@endif
