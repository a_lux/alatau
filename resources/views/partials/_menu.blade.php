<ul class="nav__list">
    @foreach ($menu as $menuItem)
        @php $menuItem = $menuItem->translate(app()->getLocale()) @endphp
        @if($_SERVER["REQUEST_URI"] == $menuItem->url)
                <li class="nav__link"><a href="#">{{ $menuItem->title }}</a></li>
        @else
                <li class="nav__link"><a href="{{ $menuItem->url }}">{{ $menuItem->title }}</a></li>
        @endif
    @endforeach
</ul>
