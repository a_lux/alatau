<div class="nav-position">
    <div class="container">
        @if($service != null)
            <a href = "{{ route('home') }}">@lang('texts.Главная') ></a>
            <a href = "{{ route('service_all') }}">@lang('texts.Услуги') ></a>
            {{ $service->translate(app()->getLocale())->name }}
        @elseif($project != null)
            <a href = "{{ route('home') }}">@lang('texts.Главная') ></a>
            <a href = "{{ route('project_all') }}">@lang('texts.Проекты') ></a>
            {{ $project->translate(app()->getLocale())->name }}
        @else
            <a href = "{{ route('home') }}">@lang('texts.Главная') ></a>
            {{ $page->getPage()->translate(app()->getLocale())->title }}
        @endif
    </div>
</div>
