@foreach($projects as $k => $v)
    <a href = "{{ route('project_inner', ['url' => $v->url]) }}" class="slider__item">
        <img src="{{ Voyager::image($v->image) }}" alt="">
        <div class="item__content">
            @if($v->name_position == 1)
                <h3 class="item__bigtitle" style="text-align: right;">{{ $v->name }}</h3>
            @elseif($v->name_position == 2)
                <h3 class="item__bigtitle" style="text-align: left;">{{ $v->name }}</h3>
            @else
                <h3 class="item__bigtitle" >{{ $v->name }}</h3>
            @endif

                <br>
            @if($v->short_description_position == 1)
                <p class="item__text" style="text-align: right;">
                    {{ $v->short_description }}
                </p>
            @elseif($v->short_description_position == 2)
                <p class="item__text" style="text-align: left;">
                    {{ $v->short_description }}
                </p>
            @else
                <p class="item__text">
                    {{ $v->short_description }}
                </p>
            @endif

        </div>
    </a>
@endforeach
