<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |Адрес
    */

    'Сфера деятельности'                                    => 'Field of activity',
    'О компании'                                            => 'About company',
    'смотреть все проекты'                                  => 'see all projects',
    'Свяжитесь с нами'                                      => 'Contact us',
    'Комментарий'                                           => 'Comment',
    'связаться с нами'                                      => 'to contact us',
    'Страница не найдена'                                   => 'Page not found',
    'Извините, страница, которую вы ищете, не найдена'      => 'Sorry, the page you are looking for was not found.',
    'Вернуться на главную'                                  => 'Go back to the main page',
    'смотреть все услуги'                                   => 'view all services',
    'подробнее'                                             => 'more details',
    'Главная'                                               => 'Home',
    'Услуги'                                                => 'Services',
    'Проекты'                                               => 'Projects',
    'Заказчик'                                              => 'Customer',
    'Проект'                                                => 'Project',
    'Цель проекта'                                          => 'Objective of the project',
    'Результат'                                             => 'Result',
    'Разработано в'                                         => 'Developed in',
];
