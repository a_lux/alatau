<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'Сфера деятельности'                                    => 'Сфера деятельности',
    'О компании'                                            => 'О компании',
    'смотреть все проекты'                                  => 'смотреть все проекты',
    'Свяжитесь с нами'                                      => 'Свяжитесь с нами',
    'Комментарий'                                           => 'Комментарий',
    'связаться с нами'                                      => 'связаться с нами',
    'Страница не найдена'                                   => 'Страница не найдена',
    'Извините, страница, которую вы ищете, не найдена'      => 'Извините, страница, которую вы ищете, не найдена.',
    'Вернуться на главную'                                  => 'Вернуться на главную',
    'смотреть все услуги'                                   => 'смотреть все услуги',
    'подробнее'                                             => 'подробнее',
    'Главная'                                               => 'Главная',
    'Услуги'                                                => 'Услуги',
    'Проекты'                                               => 'Проекты',
    'Заказчик'                                              => 'Заказчик',
    'Проект'                                                => 'Проект',
    'Цель проекта'                                          => 'Цель проекта',
    'Результат'                                             => 'Результат',
    'Разработано в'                                         => 'Разработано в',
];
