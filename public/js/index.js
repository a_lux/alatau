$(".main-slider").slick({
  arrows: true,
  slidesToShow: 1,
  dots: true,
  speed: 1000,
  autoplay: true,
  autoplaySpeed: 4000,
  cssEase: "ease-in-out",
  responsive: [
    {
      breakpoint: 1300,
      settings: {
        arrows: false,
      }
    },
    {
      breakpoint: 870,
      settings: {
        arrows: false,
        speed: 300,
      }
    },
  ]
});

$(".works-slider").slick({
  slidesToShow: 3,
  arrows: true,
  responsive: [
    {
      breakpoint: 1300,
      settings: {
        arrows: false,
      }
    },
    {
      breakpoint: 870,
      settings: {
        speed: 300,
        slidesToShow: 2,
        arrows: false,
      }
    },
    {
      breakpoint: 590,
      settings: {
        speed: 300,
        slidesToShow: 1,
        arrows: false,
      }
    },
  ]
})

$('.works-more-slider').slick({
  slidesToShow: 4,
  arrows: true,
  responsive: [
    {
      breakpoint: 1000,
      settings: {
        speed: 300,
        slidesToShow: 3,
        arrows: false,
      }
    },
    {
      breakpoint: 650,
      settings: {
        speed: 300,
        slidesToShow: 2,
        arrows: false
      }
    },
    {
      breakpoint: 500,
      settings: {
        speed: 300,
        slidesToShow: 1,
        arrows: false
      }
    }
  ]
})

$('.sertificate-slider').slick({
  slidesToShow: 3,
  centerMode: true,
  arrows: true,
  responsive: [
    {
      breakpoint: 1000,
      settings: {
        speed: 300,
        slidesToShow: 2,
        arrows: false,
      }
    },
    {
      breakpoint: 650,
      settings: {
        speed: 300,
        slidesToShow: 1,
        arrows: false
      }
    }
  ]
});

// Magnific popup
$(document).ready(function() {
  $('.slide').magnificPopup({type:'image'});
});

// $('.count').each(function () {
//   var $this = $(this);
//   jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
//     duration: 1000,
//     easing: 'swing',
//     step: function () {
//       $this.text(Math.ceil(this.Counter));
//     }
//   });
// });


const selectMap = $('.selector')
const Map_1 = $('.map-1')
const Map_2 = $('.map-2')

$(selectMap).click((elem) => {
  if(!elem.currentTarget.classList.contains('active-select')){
    selectMap.removeClass('active-select')
    elem.currentTarget.classList.add('active-select')
  }
  if(Map_1.hasClass("active-map")){
    Map_1.removeClass('active-map')
    Map_2.addClass('active-map')
  } else{
    Map_2.removeClass('active-map')
    Map_1.addClass('active-map')
  }
});



$('.hamburger').click( () => {
  $('.hamburger').toggleClass('is-active')
  $('.nav').toggleClass('nav-mobile')
  $('body').toggleClass('no-scroll')
});

$(".slick-prev").html('<img src = "/img/arrow-left.png">')
$(".slick-next").html('<img src = "/img/arrow-right.png">')

$(".sertificate-slider .slick-next").html('<img src = "/img/arrow-right-blue.png">')
$(".sertificate-slider .slick-prev").html('<img src = "/img/arrow-left-blue.png">')

$(".works-more-slider .slick-next").html('<img src = "/img/arrow-right-blue.png">')
$(".works-more-slider .slick-prev").html('<img src = "/img/arrow-left-blue.png">')
