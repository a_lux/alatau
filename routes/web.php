<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


// PAGES
Route::get('/', 'HomeController@index')->name('home');
Route::get('/lang/{url}', 'LangController@index')->name('lang');
Route::get('/about', 'AboutController@index')->name('about');

Route::get('/projects', 'ProjectController@all')->name('project_all');
Route::get('/project/{url}', 'ProjectController@index')->name('project_inner');

Route::get('/services', 'ServiceController@all')->name('service_all');
Route::get('/service/{url}', 'ServiceController@index')->name('service_inner');

Route::get('/clients', 'ClientController@index')->name('brands');
Route::get('/brands', 'BrandController@index')->name('brands');
Route::get('/contacts', 'ContactController@index')->name('contacts');

Route::get('/feedback', 'RequestController@index')->name('feedback');


