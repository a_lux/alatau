<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Project extends Model
{

    use Translatable;

    protected $translatable = ['name', 'short_description', 'customer', 'project', 'objective', 'result', 'meta_title',
        'meta_description', 'meta_keyword'];

    public static function getMain(){
        return self::where('onMain', 1)->orderBy('sort', 'ASC')->get();
    }

    public static function getLast($quantity = 8){
        return self::orderBy('sort', 'ASC')->take($quantity)->get();
    }

    public static function getAll(){
        return self::orderBy('sort', 'ASC')->get();
    }

    public static function getContent($url){
        return self::where('url', $url)->first();
    }

    public static function getByUrl($url){
        return self::where('url', $url)->first();
    }
}
