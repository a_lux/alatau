<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Client extends Model
{
    use Translatable;
    protected $translatable = ['description'];

    public static function getAll(){
        return self::orderBy('sort', 'ASC')->get();
    }
}
