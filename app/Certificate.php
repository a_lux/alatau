<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Certificate extends Model
{
    public static function getAll(){
        return self::orderBy('sort', 'ASC')->get();
    }

    public static function getHome(){
        return self::where('in_main', 1)
            ->orderBy('sort', 'ASC')
            ->get();
    }
    
}
