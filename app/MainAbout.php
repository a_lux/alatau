<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class MainAbout extends Model
{
    use Translatable;
    protected $translatable = ['first_block','second_block'];

    public static function getContent(){
        return self::first();
    }
}
