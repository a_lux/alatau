<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Page extends Model
{
    use Translatable;
    protected $translatable = ['title','meta_title','meta_description','meta_keyword'];

    public static function getByUrl($url){
        return self::where('url', $url)->first();
    }

    public function getTitle(){
        return $this->hasMany('App\Title');
    }

    public function getFirstTitle(){
        return $this->getTitle() ? $this->getTitle()->first()->translate(app()->getLocale())->title : "";
    }

    public function getFirstSubtitle(){
        return $this->getTitle() ? $this->getTitle()->first()->translate(app()->getLocale())->subtitle : "";
    }

    public function getSecondTitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(1)->first() ?
            $this->getTitle()->skip(1)->first()->translate(app()->getLocale())->title : "") : "";
    }

    public function getSecondSubtitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(1)->first() ?
            $this->getTitle()->skip(1)->first()->translate(app()->getLocale())->subtitle : "") : "";
    }

    public function getThirdTitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(2)->first() ?
            $this->getTitle()->skip(2)->first()->translate(app()->getLocale())->title : "") : "";
    }

    public function getForthTitle(){
        return $this->getTitle() ? (
            $this->getTitle()->skip(3)->first() ?
                $this->getTitle()->skip(3)->first()->translate(app()->getLocale())->title : "") : "";
    }

    public function getForthSubtitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(3)->first() ?
            $this->getTitle()->skip(3)->first()->translate(app()->getLocale())->subtitle : "") : "";
    }

}
