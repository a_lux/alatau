<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class MainBanner extends Model
{
    use Translatable;
    protected $translatable = ['title', 'content'];


    public static function getAll(){
        return self::orderBy('sort', 'ASC')->get();
    }
}
