<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 25.03.2020
 * Time: 21:58
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class FooterController extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }

}
