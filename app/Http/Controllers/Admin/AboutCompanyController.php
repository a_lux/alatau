<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 17.03.2020
 * Time: 14:16
 */

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AboutCompanyController  extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }

}
