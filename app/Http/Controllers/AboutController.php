<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.03.2020
 * Time: 14:13
 */

namespace App\Http\Controllers;


use App\AboutCompany;
use App\AboutInfographic;
use App\Certificate;
use App\Helpers\TranslatesCollection;
use App\Project;

class AboutController extends Controller
{
    public function index(){

        $model = AboutCompany::getContent();
        $graph = AboutInfographic::getAll();
        $certificates = Certificate::getAll();
        $projects = Project::getLast();

        TranslatesCollection::translate($model, app()->getLocale());
        TranslatesCollection::translate($graph, app()->getLocale());
        TranslatesCollection::translate($projects, app()->getLocale());

        return view('about.index', compact('model', 'graph', 'certificates', 'projects'));

    }
}
