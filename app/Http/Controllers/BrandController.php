<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.03.2020
 * Time: 16:30
 */

namespace App\Http\Controllers;


use App\Brand;
use App\Helpers\TranslatesCollection;

class BrandController extends Controller
{

    public function index(){

        $model = Brand::getAll();
        TranslatesCollection::translate($model, app()->getLocale());

        return view('brand.index', compact('model'));

    }

}
