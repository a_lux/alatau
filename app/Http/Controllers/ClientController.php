<?php


namespace App\Http\Controllers;

use App\Client;
use App\Helpers\TranslatesCollection;

class ClientController extends Controller
{
    public function index(){

        $model = Client::getAll();
        TranslatesCollection::translate($model, app()->getLocale());

        return view('clients.index', compact('model'));

    }
}