<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.03.2020
 * Time: 14:53
 */

namespace App\Http\Controllers;


use App\Helpers\TranslatesCollection;
use App\MainBanner;
use App\Project;

class ProjectController extends Controller
{

    public function index($url){

        $model = Project::getContent($url);
        if($model){
            TranslatesCollection::translate($model, app()->getLocale());
        }else{
            abort(404);
        }

        return view('project.index', compact('model'));

    }

    public function all(){

        $model = Project::getAll();
        $banners = MainBanner::getAll();

        TranslatesCollection::translate($model, app()->getLocale());
        TranslatesCollection::translate($banners, app()->getLocale());

        return view('project.all', compact('model', 'banners'));

    }
}
