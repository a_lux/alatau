<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.03.2020
 * Time: 13:39
 */

namespace App\Http\Controllers;


use App\Certificate;
use App\Helpers\TranslatesCollection;
use App\MainAbout;
use App\MainBanner;
use App\Project;
use App\Service;

class HomeController extends Controller
{
    public function index(){

        $banners = MainBanner::getAll();
        $about = MainAbout::getContent();
        $projects = Project::getMain();
        $services = Service::getMain();
        $certificates = Certificate::getHome();

        TranslatesCollection::translate($banners, app()->getLocale());
        TranslatesCollection::translate($about, app()->getLocale());
        TranslatesCollection::translate($projects, app()->getLocale());
        TranslatesCollection::translate($services, app()->getLocale());

        return view('home.index', compact('banners', 'about', 'projects', 'services', 'certificates'));
    }

}
