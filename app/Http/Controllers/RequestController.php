<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Request as Feedback;
use App\Mail\FeedbackRequest;


class RequestController extends Controller
{


    public function index(Request $request) {

        if($request->email != null){
            $model = Feedback::create($request->all());
            Mail::to("nurzat.kaz@gmail.com")->send(new FeedbackRequest($model));
            session()->put('request_status', 1);
            return back()->with('status', 'Profile updated!');
        }else{
            abort(404);
        }


    }





}
