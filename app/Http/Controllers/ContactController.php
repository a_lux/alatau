<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.03.2020
 * Time: 16:51
 */

namespace App\Http\Controllers;


use App\Contact;
use App\Helpers\TranslatesCollection;

class ContactController extends Controller
{
    public function index(){

        $model = Contact::getAll();
        TranslatesCollection::translate($model, app()->getLocale());

        return view('contact.index', compact('model'));

    }
}
