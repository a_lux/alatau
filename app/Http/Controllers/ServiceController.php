<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.03.2020
 * Time: 16:28
 */

namespace App\Http\Controllers;


use App\Helpers\TranslatesCollection;
use App\Service;

class ServiceController extends Controller
{
    public function index($url){

        $model = Service::getContent($url);
        if($model){
            TranslatesCollection::translate($model, app()->getLocale());
        }else{
            abort(404);
        }

        return view('service.index', compact('model'));

    }

    public function all(){

        $model = Service::getAll();
        TranslatesCollection::translate($model, app()->getLocale());

        return view('service.all', compact('model'));

    }
}
