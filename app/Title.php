<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Title extends Model
{
    use Translatable;
    protected $translatable = ['title', 'subtitle'];

}
