<?php

namespace App\Providers;

use App\Helpers\TranslatesCollection;
use App\Page\Page;
use App\Page as PageModel;
use App\Service as ServiceModel;
use App\Project as ProjectModel;
use App\Meta\MetaTag;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;

class MetaTagsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */


    protected $page;

    public function register()
    {
        $this->app->singleton(MetaTag::class, function ($app) {
            return new MetaTag(config('app.meta'));
        });

        $this->app->singleton(Page::class, function ($app) {
            return new Page();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(MetaTag $meta, Page $page)
    {
        $pageInstance = PageModel::getByUrl(Request::segment(1));
        $projectInstance = ProjectModel::getByUrl(Request::segment(2));
        $serviceInstance = ServiceModel::getByUrl(Request::segment(2));
        $instance = null;

        if($serviceInstance){
            $instance = $serviceInstance;
        }elseif($projectInstance){
            $instance = $projectInstance;
        }else if($pageInstance) {
            $instance = $pageInstance;
        }

        if($instance != null){
            $page->setPage($instance);
            if($instance->meta_title){
                TranslatesCollection::translate($instance, app()->getLocale());
                $meta->setTitle($instance->meta_title);
                $meta->setDescription($instance->meta_description);
                $meta->setKeyword($instance->meta_keyword);
            }
        }

        $page->setMeta($meta);
    }
}
