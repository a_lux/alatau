<?php

namespace App\Providers;



use App\Footer;
use App\Helpers\TranslatesCollection;
use App\Page\Page;
use App\Project;
use App\Service;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Page $page, Service $service, Project $project, Footer $footer)
    {

        View::composer('*', function($view) use($page) {
            $view->with(['page' => $page]);
        });


        View::composer('*', function($view) use($service) {
            $view->with(['service' => $service::getByUrl( Request::segment(2))]);
        });

        View::composer('*', function($view) use($project) {
            $view->with(['project' => $project::getByUrl( Request::segment(2))]);
        });

        View::composer('*', function($view) use($footer) {
            $footer = $footer::getContent();
            TranslatesCollection::translate($footer, app()->getLocale());
            $view->with(['footer' => $footer]);
        });


    }
}
