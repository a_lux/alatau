<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class AboutCompany extends Model
{
    use Translatable;
    protected $translatable = ['content', 'services', 'areas_of_activity'];

    public static function getContent(){
        return self::first();
    }

}
