<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class AboutInfographic extends Model
{
    use Translatable;
    protected $translatable = ['title', 'subtitle', 'content'];

    public static function getAll(){
        return self::orderBy('sort', 'ASC')->get();
    }
}
