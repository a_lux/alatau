<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Service extends Model
{
    use Translatable;
    protected $translatable = ['name', 'content', 'meta_title', 'meta_description', 'meta_keyword'];

    public static function getMain(){
        return self::orderBy('sort', 'asc')->take(8)->get();
    }

    public static function getAll(){
        return self::orderBy('sort', 'ASC')->get();
    }

    public static function getContent($url){
        return self::where('url', $url)->first();
    }

    public static function getByUrl($url){
        return self::where('url', $url)->first();
    }
}
